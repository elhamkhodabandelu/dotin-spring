package SpringTest;


import AppModel.CompactDisc;
import config.ConfigApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConfigApplication.class)
public class TestConfig {
    @Autowired
    private CompactDisc compactDisc;

    @Test
    public void compactSiscPlayShouldNotBeNull() {
        compactDisc.compactSiscPlay();
        assertThat(compactDisc, is(notNullValue()));
}
}
