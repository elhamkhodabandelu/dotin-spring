package AppModel;

import org.springframework.stereotype.Component;

@Component
public class SgtPeppers implements CompactDisc {

    private String albumName = "darham";
    private String artistName = "Reza Yazdani";

    public void compactSiscPlay() {
        System.out.println("playing " + albumName + " from " + artistName);
    }
}
