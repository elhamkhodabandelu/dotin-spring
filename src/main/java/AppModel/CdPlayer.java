package AppModel;


import org.springframework.stereotype.Component;

@Component
public class CdPlayer implements MediaPlayer {

    private CompactDisc compactDisc;

    public CdPlayer(CompactDisc compactDisc) {
        this.compactDisc = compactDisc;
    }

    public void mediaPlayerPlay() {
        compactDisc.compactSiscPlay();
    }
}
